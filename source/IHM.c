

/**********************************************************************/
/** Spécification des procédures et fonctions données dans le header **/
/**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include "IHM.h"


int plateau[TAILLE_X][TAILLE_Y];
int grille[11][11];
etat etat_jeu;


SDL_Color fontBlack={0,0,0};

void init_plateau(int p[TAILLE_X][TAILLE_Y])
{
	int x,y;
	for(x=0;x!=TAILLE_X;x++){
		for(y=0;y!=TAILLE_Y;y++){
			p[x][y]=0;
		}
	}
}

void init_grille(int p[11][11])
{
	int x,y;
	for(x=0;x!=11;x++){
		for(y=0;y!=11;y++){
			if(x==5 && y==5) p[x][y]=1;
			else p[x][y]=0;
		}
	}
}

void effacer_plateau()
{
	int x,y;
	for(x=0;x!=TAILLE_X;x++){
		for(y=0;y!=TAILLE_Y;y++){
			plateau[x][y]=0;
		}
	}
}

void applique_masque(int X,int Y)
{
	int x,y;
	for(x=-5;x!=6;x++){
		for(y=-5;y!=6;y++){
			if(pion_valide(X+x,Y+y)){
				switch(grille[5+x][5+y])
				{
					case 1: plateau[X+x][Y+y]=1; break;
					case 2: plateau[X+x][Y+y]=0; break;
					default: break;
				}
			}
		}
	}
}

int clic_menu(int X,int Y)
{
	int x,y;
	int retour=0;
	if(reco_clic_complet(X,Y,POSX_PLATEAU,POSY_PLATEAU,1001,601) && !etat_jeu.cadenas){
		x=(X-POSX_PLATEAU-1)/20;
		y=(Y-POSY_PLATEAU-1)/20;
		applique_masque(x,y);
		effacer();
		affichage_plateau(plateau);
		affichage_grille(grille);
		affichage_menu(etat_jeu.play,etat_jeu.vitesse,etat_jeu.cadenas);
	}
	if(reco_clic(X,Y,POSX_PLAY,POSY_PLAY)){
		etat_jeu.play=(etat_jeu.play+1)%2;
		retour=1;
	}
	if(reco_clic(X,Y,POSX_VIT,POSY_VIT)){
		if(etat_jeu.vitesse==4) etat_jeu.vitesse=1;
		else etat_jeu.vitesse++;
		retour=1;
	}
	if(reco_clic(X,Y,POSX_QUITTER,POSY_QUITTER)) etat_jeu.continuer=0;
	if(reco_clic(X,Y,POSX_EFFACER,POSY_EFFACER)){
		effacer_plateau();
		retour=1;
	}
	if(reco_clic(X,Y,POSX_CAD,POSY_CAD)){
		etat_jeu.cadenas=(etat_jeu.cadenas+1)%2;
		retour=1;
	}
	if(reco_clic_complet(X,Y,POSX_GRILLE,POSY_GRILLE,221,221)){
		x=(X-POSX_GRILLE-1)/20;
		y=(Y-POSY_GRILLE-1)/20;
		if(grille[x][y]==2){
			grille[x][y]=0;
			effacer();
			affichage_plateau(plateau);
			affichage_grille(grille);
			affichage_menu(etat_jeu.play,etat_jeu.vitesse,etat_jeu.cadenas);
		}else{ 
			grille[x][y]=(grille[x][y]+1)%3;
			if(grille[x][y]==1) rajout_pion_grille(x,y);
			else rajout_pion_grille_eff(x,y);
		}	
		if(x==5 && y==5){
			posPion.x = POSX_GRILLE+20*x+1;
			posPion.y = POSY_GRILLE+20*y+1;
			SDL_BlitSurface(pionCentre,NULL,ecran,&posPion);
		}
			
	}
	if(reco_clic(X,Y,POSX_EFFAC,POSY_EFFAC)){
		init_grille(grille);
		retour=1;
	}
	return retour;
}

// Procedure_principal équivalent au main
void procedure_principale()
{
	
    SDL_Event event; 
    init_SDL_global();
    SDL_Flip(ecran);
    srand(time(NULL));
    
    int ta=0,tp=0,tt;
  
    
    etat_jeu.continuer=1;
    etat_jeu.etat_logiciel=0;
    etat_jeu.vitesse=1;
    etat_jeu.cadenas=0;
    etat_jeu.play=0;
	init_plateau(plateau);
	init_grille(grille);
    
    effacer();
    affichage_plateau(plateau);
    affichage_grille(grille);
    affichage_menu(etat_jeu.play,etat_jeu.vitesse,etat_jeu.cadenas);
    SDLKey key_pressed ;
    while (etat_jeu.continuer)
    {
		
		if(etat_jeu.play) {
			ta=SDL_GetTicks();
			if(etat_jeu.vitesse==1) tt=1000;
			if(etat_jeu.vitesse==2) tt=750;
			if(etat_jeu.vitesse==3) tt=500;
			if(etat_jeu.vitesse==4) tt=250;
			if(ta-tp>tt){
				jouer(plateau);
				effacer();
				affichage_plateau(plateau);
				affichage_grille(grille);
				affichage_menu(etat_jeu.play,etat_jeu.vitesse,etat_jeu.cadenas);
				tp=ta;
			}
		}
        if(SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT)
                    {   
                        int clicX = event.motion.x;
                        int clicY = event.motion.y;
                             
						switch(etat_jeu.etat_logiciel)
						{
							case 0:
								if(clic_menu(clicX,clicY)){
									effacer();
									affichage_plateau(plateau);
									affichage_grille(grille);
									affichage_menu(etat_jeu.play,etat_jeu.vitesse,etat_jeu.cadenas);
								}
							default: break;
						}
                    }
                    break;
                case SDL_KEYDOWN:
                    key_pressed = event.key.keysym.sym; // on récupère la touche
                    if(key_pressed==SDLK_ESCAPE) etat_jeu.continuer=false;
                    break;
                case SDL_QUIT: etat_jeu.continuer = false;break;
                default:break;
            }
        }
        SDL_Flip(ecran);
    }
    SDL_FreeSurface(ecran);
}
