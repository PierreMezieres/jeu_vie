/***********************************************************
 * Fichier global regroupant:
 * -des types définis
 * -des fonctions et procédure
 * Utile à tous les modules
 * Comme tous les modules ne sont pas inclus les
 * uns dans les autres. Nous avons choisis de faire un
 * fichier global qui regroupe les types et les fonctions
 * utilisé dans ces différents modules.       
 ***********************************************************/

#ifndef __global__
#define __global__

#define TAILLE_X 50
#define TAILLE_Y 30

#include <time.h>

/*************************** Type définis *****************************/

/* contenue du plateau temporaire pour la recherche de chemin */
typedef struct s_casejeu{
	int content;
	int color;
	int vu;
}casejeu;

/* structure permettant de ranger des coups */
typedef struct{
    int joueur;
    int caseX;
    int caseY;
} coup;

/* pile utilisé pour la recherche de chemin */
typedef struct Pile_s{
	coup tab_coup[200];
	int nb_elem;
}pile;

/* Structure de l'IA */
typedef struct coupIA{
	coup dernier_coup;
	int color;
	int niv;
	pile pil;
	int perdu;
}coupIA; 

/* Donnée utile dans tous les module
 * -Plateau de jeu (0=rien,1=rouge,2=bleu)
 * -Dernier coup joué */
typedef struct{
    int plateau[TAILLE_X][TAILLE_Y];
    coup dernier_coup;
} data; 

/* Structure qui contient les éléments principaux 
 * pour connaitre l'état du jeu */
typedef struct
{
	int vitesse;
	int cadenas;
	int play;
	int confirmer;
	int etat_logiciel; //etat du menu
	int choix; //choix de l'utilisateur
	int continuer; //Fin du programme
	int joueur; //Joueur qui doit jouer
	int fin; //Fin de partie
	int mode; //mode de jeu choisie(*note en dessous)
	char ch[50]; //chaine auxilliaire
	int hist[3][130]; //Historique de la partie
	int length_h; //Longueur de l'historique
	coupIA ia1;
	coupIA ia2;
}etat;

/* *le mode de jeu choisie:
 * -1=HvsH-
 * -2=HvsIA1(humain=Rouge)
 * -3=HvsIA1(humain=Bleu)
 * -4=HvsIA2(humain=Rouge)
 * -5=HvsIA2(humain=bleu)
 * -6=IA1vsIA1 
 * -7=IA1vsIA2
 * -8=IA2vsIA2 */
	
/**********************************************************************/

 /** procédure et fonction pour la gestion de la pile **/
 void init_pile(pile *p); //initialisation
	 
 int pile_vide(pile p); //Test de la pile vide
 
 void empiler(pile *p,coup c); //On empile un coup
 
 coup depiler(pile *p); //On dépile un coup
 
 coup tete(pile p); //On renvoie la tête
 
 int num_tete(pile p); //On renvoie le numéro de la tête
 
 int i_bon(pile p,int i); //Regarde si l'élément n°i existe
  
 coup accede_i(pile p,int i); //renvoie le coup n°i
 
 int appartient(pile p,int x,int y); //On regarde si le coup en x y est dans la pile
 /** *********************************************** **/

#endif
