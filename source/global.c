#include "global.h" 
#include <stdio.h>

 /********* Utilitaire pour la pile *********/
 void init_pile(pile *p){
	 p->nb_elem=-1;
 }
	 
 int pile_vide(pile p){
	 return p.nb_elem==-1;
 }
 
 void empiler(pile *p,coup c){
	 p->nb_elem++;
	 p->tab_coup[p->nb_elem]=c;
 }
 
 coup depiler(pile *p){
	 coup c=p->tab_coup[p->nb_elem];
	 p->nb_elem--;
	 return c;
 }
 
 int contenu(pile p,int x,int y){
	 int i;
	 for(i=0;i!=p.nb_elem;i++){
		 if(p.tab_coup[i].caseX==x && p.tab_coup[i].caseY==y) return 1;
	 }
	 return 0;
 }
 
 coup tete(pile p){
	 return p.tab_coup[p.nb_elem];
 }
 
 int num_tete(pile p){
	 return p.nb_elem-1;
 }
 
 int i_bon(pile p,int i){
	 return i>p.nb_elem;
 }
 
 coup accede_i(pile p,int i){
	 return p.tab_coup[i];
 }
 
 void affichage_pile(pile p){
	 int i=0; coup cou;
	 printf("pile:\n");
	 while(!i_bon(p,i)){
		 cou=accede_i(p,i);
		 printf("i=%d x=%d y=%d j=%d\n",i,cou.caseX,cou.caseY,cou.joueur);
		 i++;
	 }
 }
 
 int appartient(pile p,int x,int y){
	 int i=0; coup cou;
	 while(!i_bon(p,i)){
		 cou=accede_i(p,i);
		 i++;
		 if(cou.caseX==x && cou.caseY==y) return 1;
	 }
	 return 0;
 }


