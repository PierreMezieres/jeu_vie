/**********************************************************************
 * Auteurs : Nicolas Pélissier
 * 
 * But: gère tout ce qui est propre au Jeu :                     
 *        - Determine si un coup est valide                                  
 *        - Determine si un joueur a gagné la partie                         
 *       
 * Dépendances: Dépends du module "ia"
 * 				et global.h qui regroupe les type
 *    			et fonctions utile à tous les modules.
 *                                                              
 * Note : Code implanté par Nicolas Pélissier,
 * 		  Modification apporté par Pierre Mézières      
 *                                                                    
 * ********************************************************************/

#ifndef __JEU_H__
#define __JEU_H__

#include "global.h"

void jouer(int plateau[TAILLE_X][TAILLE_Y]);

int pion_valide(int x,int y);



#endif //JEU_H
