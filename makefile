CC=gcc
OPT=-Wall -g -std=c99
EXEC=vie
OPT2=`sdl-config --cflags --libs` -lSDL_image -lSDL_ttf

all: $(EXEC) 

point_o/global.o : source/global.c
	$(CC) -o point_o/global.o -c source/global.c $(OPT)

point_o/jeu.o : source/jeu.c
	$(CC) -o point_o/jeu.o -c source/jeu.c $(OPT)
	
point_o/fonctionnalite.o : source/fonctionnalite.c
	$(CC) -o point_o/fonctionnalite.o -c source/fonctionnalite.c $(OPT)
	
point_o/interface.o : source/interface.c
	$(CC) -o point_o/interface.o -c source/interface.c $(OPT) $(OPT2)
	
point_o/IHM.o : source/IHM.c
	$(CC) -o point_o/IHM.o -c source/IHM.c $(OPT) $(OPT2)

point_o/main.o : source/main.c
	$(CC) -o point_o/main.o -c source/main.c $(OPT) $(OPT2)
	
$(EXEC): point_o/global.o point_o/jeu.o point_o/fonctionnalite.o point_o/interface.o point_o/IHM.o point_o/main.o
	$(CC) point_o/global.o point_o/jeu.o point_o/fonctionnalite.o point_o/interface.o point_o/IHM.o point_o/main.o -o $(EXEC) $(OPT2) $(OPT)
	
	@echo ""
	@echo ""
	@echo "**************************************************"
	@echo "************** Compilation terminer **************"
	@echo "**************************************************"
	@echo ""

	
clean:
	rm -rf point_o/*.o $(EXEC) 
